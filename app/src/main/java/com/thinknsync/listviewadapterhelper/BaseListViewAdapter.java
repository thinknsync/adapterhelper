package com.thinknsync.listviewadapterhelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.thinknsync.observerhost.ObserverHost;
import com.thinknsync.observerhost.ObserverHostImpl;
import com.thinknsync.observerhost.TypedObserver;

import java.util.List;

public abstract class BaseListViewAdapter<T> extends ArrayAdapter<T> implements ObserverHost<T> {

    protected List<T> objects;
    private ObserverHostImpl<T> observerHost;


    public BaseListViewAdapter(Context context, List<T> objects) {
        super(context, 0, objects);
        this.observerHost = new ObserverHostImpl<>();
        this.objects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(getView(), parent, false);
        }

        setValues(convertView, getItem(position));

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = getView(position, convertView, parent);
        return view;
    }

    protected abstract void setValues(View convertView, T listObject);
    protected abstract int getView();

    @Override
    public void addToObservers(TypedObserver<T> typedObserver) {
        observerHost.addToObservers(typedObserver);
    }

    @Override
    public void addToObservers(List<TypedObserver<T>> list) {
        observerHost.addToObservers(list);
    }

    @Override
    public void removeObserver(TypedObserver<T> typedObserver) {
        observerHost.removeObserver(typedObserver);
    }

    @Override
    public void clearObservers() {
        observerHost.clearObservers();
    }

    @Override
    public void notifyObservers(T data) {
        observerHost.notifyObservers(data);
    }

    @Override
    public void notifyError(Exception e) {
        observerHost.notifyError(e);
    }
}
