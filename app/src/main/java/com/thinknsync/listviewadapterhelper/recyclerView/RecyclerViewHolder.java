package com.thinknsync.listviewadapterhelper.recyclerView;

import com.thinknsync.objectwrappers.FrameworkWrapper;

public interface RecyclerViewHolder {
    void initView(FrameworkWrapper viewWrapper);
    FrameworkWrapper getContextWrapper();
}
