package com.thinknsync.listviewadapterhelper.recyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;


import com.thinknsync.observerhost.ObserverHost;
import com.thinknsync.observerhost.ObserverHostImpl;
import com.thinknsync.observerhost.TypedObserver;

import java.util.List;

public abstract class BaseRecyclerViewAdapter<T, U extends RecyclerView.ViewHolder & RecyclerViewHolder>
        extends RecyclerView.Adapter<U> implements ObserverHost<T> {

    protected Context context;
    protected List<T> objects;
    private ObserverHost<T> observerHost;

    public BaseRecyclerViewAdapter(Context context, List<T> objectList){
        this.context = context;
        this.objects = objectList;
        this.observerHost = new ObserverHostImpl<>();
    }

    @Override
    public U onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView = LayoutInflater.from(parent.getContext()).inflate(getView(), parent,false);
        return getViewHolder(itemView);
    }

    protected abstract U getViewHolder(View itemView);
    protected abstract int getView();

    @Override
    public void onBindViewHolder(U holder, final int position){
        setValues(holder, objects.get(position));
    }

    protected abstract void setValues(U holder, T object);

    @Override
    public int getItemCount(){
        return objects.size();
    }

    @Override
    public void addToObservers(TypedObserver<T> typedObserver) {
        observerHost.addToObservers(typedObserver);
    }

    @Override
    public void addToObservers(List<TypedObserver<T>> list) {
        observerHost.addToObservers(list);
    }

    @Override
    public void removeObserver(TypedObserver<T> typedObserver) {
        observerHost.removeObserver(typedObserver);
    }

    @Override
    public void clearObservers() {
        observerHost.clearObservers();
    }

    @Override
    public void notifyObservers(T t) {
        observerHost.notifyObservers(t);
    }

    @Override
    public void notifyError(Exception e) {
        observerHost.notifyError(e);
    }
}
