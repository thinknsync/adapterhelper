package com.thinknsync.listviewadapterhelper.recyclerView;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.AndroidViewWrapper;
import com.thinknsync.objectwrappers.FrameworkWrapper;


public abstract class BaseViewHolder extends RecyclerView.ViewHolder implements RecyclerViewHolder {

    private FrameworkWrapper contextWrapper;

    public BaseViewHolder(@NonNull final View itemView) {
        super(itemView);
        this.contextWrapper = new AndroidContextWrapper(itemView.getContext());
        initView(new AndroidViewWrapper(itemView));
    }

    @Override
    public FrameworkWrapper getContextWrapper() {
        return contextWrapper;
    }
}
