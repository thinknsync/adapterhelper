package com.thinknsync.listviewadapterhelper.recyclerView;

public interface SelectableRecyclerViewHolder extends RecyclerViewHolder {
    void setItemSelected();
    void setItemUnSelected();
}
