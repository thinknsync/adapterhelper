package com.thinknsync.listviewadapterhelper.recyclerView;

import android.content.Context;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.thinknsync.listviewadapterhelper.SelectableListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class BaseSelectableRecyclerViewAdapter<T, U extends RecyclerView.ViewHolder & SelectableRecyclerViewHolder>
        extends BaseRecyclerViewAdapter<T, U> implements SelectableListAdapter<T> {

    protected int defaultColor = DEFAULT_COLOR;
    protected int selectedColor = SELECTED_COLOR;
    protected int errorColor = ERROR_COLOR;

    protected HashMap<T, U> allVisibleViews;
    protected List<T> backgroundColorChangeElements;
    protected boolean shouldColor = true;
    protected boolean allowMultiSelect = true;

    public BaseSelectableRecyclerViewAdapter(Context context, List<T> objectList) {
        super(context, objectList);
        backgroundColorChangeElements = new ArrayList<>();
        allVisibleViews = new HashMap<>();
    }

    @Override
    public void onBindViewHolder(final U holder, final int position) {
        super.onBindViewHolder(holder, position);
        allVisibleViews.put(objects.get(position), holder);
        if(shouldColor) {
            if(isColorChangeRequired(objects.get(position))){
                holder.setItemSelected();
            } else {
                holder.setItemUnSelected();
            }
        }
    }

    @Override
    public void setBackgroundColorChangeElements(List<T> changeBackgroundObjects) {
        this.backgroundColorChangeElements = changeBackgroundObjects;
        notifyDataSetChanged();
    }

    @Override
    public void addToBackgroundColorChangeElements(T changeBackgroundObject) {
        if(!allowMultiSelect){
            this.backgroundColorChangeElements.clear();
        }
        this.backgroundColorChangeElements.add(changeBackgroundObject);
        notifyDataSetChanged();
    }

    @Override
    public void removeFromBackgroundColorChangeElements(T object) {
        backgroundColorChangeElements.remove(object);
        notifyDataSetChanged();
    }

    @Override
    public void setSelectedColor(int color) {
        selectedColor = color;
    }

    @Override
    public void setDefaultColor(int color) {
        defaultColor = color;
    }

    @Override
    public void setErrorColor(int color) {
        errorColor = color;
    }

    @Override
    public void changeBackgroundColorOfItem(View itemView, int color) {
        itemView.setBackgroundColor(color);
    }

    @Override
    public void setShouldColor(boolean shouldColor) {
        this.shouldColor = shouldColor;
    }

    @Override
    public void setAllowMultiSelect(boolean allowMultiSelect) {
        this.allowMultiSelect = allowMultiSelect;
    }
}
